package com.eafc.vanass.olympic;


/*
    === JEUX OLYMPIQUES ===   

    Créez une classe "Character" qui aura comme attributs : 
        - id [int]
        - name [String]
        - country [enum Country]

    Créez une classe "Athlete" qui héritera de la classe "Character"
        , avec comme attributs spécifiques :
        - strength [byte 0, 10]
        - dexterity [byte 0, 10]
        - endurance [byte 0, 10]
        - pace = 0 [byte 0, 10] // doit TOUJOURS être à 0 à l'instanciation
        - morale [byte 0, 10]
        - stamina [byte 0, 10]
        - banned = false [bool] // indique s'il est banni de la compétition
        - rest = true [bool] // indique s'il est au repos ou en compétition
        , avec comme méthode spécifique :
        - dope() 
            , dont le comportement augmentera strength, dexterity, endurance,
                morale et stamina de 5
        - run() en tant que méthode abstraite

    Créez une classe "Sprinter" qui héritera de la classe "Athlete"
        , avec comme attributs spécifiques :
        - acceleration [byte 0, 10]
        - speed [byte 0, 10]
        - maxspeed [byte 0, 10]
        - distance = 0 [int 0, 42195]
        - round = 0 [short]
        , avec comme méthode spécifique :
        - run(Race race) // à réaliser lorsque la classe Race sera créée
           , dont le comportement modifiera l'attribut "rest" à false
            augmentera pace de acceleration et speed et distance de pace.
            A aucun moment pace ne peut excéder maxspeed.
            Si la distance de Sprinter est inférieure à la distance de Race,
            round de Sprinter sera augmenté de 1.

    Créez une classe "Competition" qui aura comme attributs :
        - name [String]
        , avec comme méthode spécifique :
        - init() en tant que méthode abstraite

    Créez une classe "Race" qui héritera de la classe "Competition"
        , avec comme attributs spécifiques :
        - id [int] // représente l'instance de la course
        - distance [int 60, 42195]
        - athletes [array of int] // vérifiez qu'il s'agit de Athlete.id valides
        , avec comme méthode spécifique :
        - init() // lance la course
            , dont le comportement exécutera la méthode run de chaque athlète
            assigné à la course jusqu'à ce que la distance de la course soit
            atteinte par chaque athlète.

    Créez une classe "Judge" qui héritera de la classe "Character"
        , avec comme attributs spécifiques :
        - judgment [int 0, 10]
        - ethic [int 0, 10]
        , avec comme méthode spécifique :
        - exclude(Athlete athlete)
            , dont le comportement modifiera l'attribut banned de l'athlete
            passé en paramètre à TRUE
        - control(Athlete athlete)
            , dont le comportement fera appel à la méthode exclude ciblant
            l'athlete passé en paramètre

*/

public class Launcher {

    public static void main(String[] args) {
        /*
            Créez 1 Course, 1 Juge et 8 Sprinters
            Lancez une Course et affichez les résultats
            Les résultats comprendront : 
   		Le nom de la course ainsi que sa distance.
		Le nom du juge ainsi que son pays.
        	Le nom, le pays et la distance parcourue de chaque athlète participant à la course.       
        */
        Judge juge = new Judge((byte)5, (byte)5, "Juge 1", Country.FR);
        Sprinter[] competitors = {
            new Sprinter((byte)6, (byte)5, (byte)8, (byte)5, (byte)5, (byte)4, (byte)1, (byte)7, "Sprinter 1", Country.BE),
            new Sprinter((byte)3, (byte)6, (byte)9, (byte)5, (byte)5, (byte)6, (byte)9, (byte)3, "Sprinter 2", Country.FR),
            new Sprinter((byte)8, (byte)7, (byte)6, (byte)5, (byte)5, (byte)2, (byte)6, (byte)4, "Sprinter 3", Country.UK),
            new Sprinter((byte)4, (byte)8, (byte)7, (byte)5, (byte)5, (byte)3, (byte)2, (byte)8, "Sprinter 4", Country.DE),
            new Sprinter((byte)6, (byte)6, (byte)7, (byte)5, (byte)5, (byte)5, (byte)4, (byte)8, "Sprinter 5", Country.FR),
            new Sprinter((byte)5, (byte)9, (byte)8, (byte)5, (byte)5, (byte)3, (byte)3, (byte)7, "Sprinter 6", Country.DE),
            new Sprinter((byte)4, (byte)8, (byte)4, (byte)5, (byte)5, (byte)6, (byte)7, (byte)6, "Sprinter 7", Country.BE),
            new Sprinter((byte)2, (byte)9, (byte)6, (byte)5, (byte)5, (byte)7, (byte)5, (byte)4, "Sprinter 8", Country.LU)
        };
        
        Race hectometer = new Race(100, competitors, "100 mètres");
        hectometer.init();
        System.out.println(hectometer);
        System.out.println(juge);
    }
}
