/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.eafc.vanass.olympic;

/**
 *
 * @author binot
 */
public class Judge extends Character {
    
    protected byte judgment;
    protected byte ethic;

    // constructor
    public Judge(byte judgment, byte ethic, String name, Country country) {
        super(name, country);
        this.judgment = judgment;
        this.ethic = ethic;
    }

    // Getters
    public byte getJudgment() {
        return judgment;
    }

    public byte getEthic() {
        return ethic;
    }

    // Setters
    public void setJudgment(byte judgment) {
        this.judgment = this.minMax(judgment, 0, 10);
    }

    public void setEthic(byte ethic) {
        this.ethic = this.minMax(ethic, 0, 10);
    }

    @Override
    public String toString() {
        return "Judge{" + "name = " + name + ", country=" + country + "}";
    }
}
