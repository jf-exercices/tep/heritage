package com.eafc.vanass.olympic;

public class Race extends Competition {
    
    public int id;
    public int distance;
    public Sprinter[] athletes;

    public Race(int distance, Sprinter[] athletes, String name) {
        super(name);
        this.distance = distance;
        this.athletes = athletes;
    }

    // Getters
    public int getId() {
        return id;
    }

    public int getDistance() {
        return distance;
    }

    public Sprinter[] getAthletes() {
        return athletes;
    }

    // Setters
    public void setId(int id) {
        this.id = id;
    }

    public void setDistance(int distance) {
        if (distance < 60) {
            distance = 60;
        } else if (distance > 42195) {
            distance = 42195;
        }
        this.distance = distance;
    }

    public void setAthletes(Sprinter[] athletes) {
        this.athletes = athletes;
    }
    

    @Override
    public void init() {
        for (Sprinter runner : this.athletes) {
            runner.run(this);
        }
        this.output();
    }
    
    public void output() {
        for (Sprinter runner : this.athletes) {
            System.out.println(runner);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Race{");
        sb.append("name=").append(name);
        sb.append(", distance=").append(distance);
        sb.append('}');
        return sb.toString();
    }

    
}
