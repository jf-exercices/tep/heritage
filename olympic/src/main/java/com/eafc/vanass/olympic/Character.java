package com.eafc.vanass.olympic;

public abstract class Character {
    public int id;
    public String name;
    public Country country;

    // Constructor
    public Character(String name, Country country) {
        this.name = name;
        this.country = country;
    }

    // Getters
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Country getCountry() {
        return country;
    }

    // Setters
    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
    
    protected byte minMax(byte value, int min, int max) {
        if (value >= min && value <= max) {
            return value;
        } else {
            return (byte) min;
        }
    }
}
