package com.eafc.vanass.olympic;

public abstract class Athlete extends Character {
    
    protected byte strength;
    protected byte dexterity;
    protected byte endurance;
    protected byte pace = 0;
    protected byte morale;
    protected byte stamina;
    protected boolean banned = false;
    protected boolean rest = true;

    public Athlete(byte strength, byte dexterity, byte endurance, byte morale, byte stamina, String name, Country country) {
        super(name, country);
        this.strength = strength;
        this.dexterity = dexterity;
        this.endurance = endurance;
        this.morale = morale;
        this.stamina = stamina;
    } 

    // Getters
    public byte getStrength() {
        return strength;
    }

    public byte getDexterity() {
        return dexterity;
    }

    public byte getEndurance() {
        return endurance;
    }

    public byte getPace() {
        return pace;
    }

    public byte getMorale() {
        return morale;
    }

    public byte getStamina() {
        return stamina;
    }

    public boolean isBanned() {
        return banned;
    }

    public boolean isRest() {
        return rest;
    }

    // Setters
    public void setStrength(byte strength) {
        this.strength = this.minMax(strength, 0, 10);
    }

    public void setDexterity(byte dexterity) {
        this.dexterity = this.minMax(dexterity, 0, 10);
    }

    public void setEndurance(byte endurance) {
        this.endurance = this.minMax(endurance, 0, 10);
    }

    public void setPace(byte pace) {
        this.pace = this.minMax(pace, 0, 10);
    }

    public void setMorale(byte morale) {
        this.morale = this.minMax(morale, 0, 10);
    }

    public void setStamina(byte stamina) {
        this.stamina = this.minMax(stamina, 0, 10);
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }

    public void setRest(boolean rest) {
        this.rest = rest;
    }
      
    public void dope() {
        strength += 5;
        dexterity += 5;
        endurance += 5;
        morale += 5;
        stamina += 5;
    }

    public abstract void run(Race race);
}
