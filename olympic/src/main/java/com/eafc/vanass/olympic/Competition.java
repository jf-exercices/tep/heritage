package com.eafc.vanass.olympic;

public abstract class Competition {
        public String name;
        
        public Competition(String name) {
            this.name = name;
        }
            
        public abstract void init();
}
