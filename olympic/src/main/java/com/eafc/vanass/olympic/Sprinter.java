package com.eafc.vanass.olympic;

import java.util.concurrent.ThreadLocalRandom;

public class Sprinter extends Athlete {
    
    protected byte acceleration;
    protected byte speed;
    protected byte maxspeed;
    protected int distance = 0;
    protected short round = 0;

    public Sprinter(byte acceleration, byte speed, byte maxspeed, byte strength, byte dexterity, byte endurance, byte morale, byte stamina, String name, Country country) {
        super(strength, dexterity, endurance, morale, stamina, name, country);
        this.acceleration = acceleration;
        this.speed = speed;
        this.maxspeed = maxspeed;
    }

    // Getters
    public byte getAcceleration() {
        return acceleration;
    }

    public byte getSpeed() {
        return speed;
    }

    public byte getMaxspeed() {
        return maxspeed;
    }

    public int getDistance() {
        return distance;
    }

    public short getRound() {
        return round;
    }

    // Setters
    public void setAcceleration(byte acceleration) {
        this.acceleration = this.minMax(acceleration, 0, 10);
    }

    public void setSpeed(byte speed) {
        this.speed = this.minMax(speed, 0, 10);
    }

    public void setMaxspeed(byte maxspeed) {
        this.maxspeed = this.minMax(maxspeed, 0, 10);
    }

    public void setDistance(int distance) {
        if (distance < 0) {
            distance = 0;
        } else if (distance > 42195) {
            distance = 42195;
        }
        this.distance = distance;
    }

    public void setRound(short round) {
        this.round = round;
    }    
        
    @Override
    public void run(Race race) {
        while (this.distance < race.distance) {
            this.rest = false;
            if (this.stamina < this.endurance) {
                this.pace = 2;
            } else {
                //this.pace += this.acceleration + this.speed;
                this.pace += ThreadLocalRandom.current().nextInt(0, this.acceleration + 1) + ThreadLocalRandom.current().nextInt(0, this.speed + 1);
            }
            if (this.pace > this.maxspeed) {
                this.pace = this.maxspeed;
                this.stamina -= 1;
            }
            this.distance += this.pace;
            this.round += 1;            
        }
        this.rest = true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Sprinter{");
        sb.append("name=").append(name);
        sb.append(", country=").append(country);
        sb.append(", pace=").append(pace);
        sb.append(", acceleration=").append(acceleration);
        sb.append(", speed=").append(speed);
        sb.append(", maxspeed=").append(maxspeed);
        sb.append(", endurance=").append(endurance);
        sb.append(", stamina=").append(stamina);
        sb.append(", distance=").append(distance);
        sb.append(", round=").append(round);
        sb.append('}');
        return sb.toString();
    }

}
